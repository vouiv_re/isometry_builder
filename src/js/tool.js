"use strict";

let radioGroups = document.getElementsByClassName("radio");

export function selectRadio() {
    for (let i = 0; i < radioGroups.length; i++) {
        let radioElements = radioGroups[i].children;
        for (let j = 0; j < radioElements.length; j++) {
            radioElements[j].addEventListener("click", () => {
                for (let k = 0; k < radioElements.length; k++) {
                    radioElements[k].classList.remove("selected");
                }
                console.log(radioElements[j].dataset.value);
                radioElements[j].classList.add("selected");
            });
        }
    }
}

/**
 * Create HTML element with possibly content, id, class and children 
 *
 * @param string type
 * @param string content
 * @param string id
 * @param string CSSClass
 * @param array children
 */
 export function createElt(type = "p", content = null, id = null, CSSClass = null, children = null) {
    let elt = document.createElement(type);
    if (content !== null) elt.textContent = content;
    if (id !== null) elt.setAttribute("id", id);
    if (CSSClass !== null) elt.className = CSSClass;
    if (children !== null) {
        children.forEach(element => {
            elt.appendChild(element)
        });
    }

    return elt;
}

/**
 * Builds, displays then removes an informational pop-up
 *
 * @param string text
 * @param string skin
 */
export function displayPopup(text, skin = "dark") {
    let popupSection = document.getElementById('popup-section');
    let div = document.createElement('div');
    let p = document.createElement('p');
    let cross = document.createElement('div');
    let i = document.createElement('i');
    i.className = "fas fa-times";
    div.classList.add(skin);
    cross.classList.add("cross");
    p.innerHTML = text;
    cross.appendChild(i);
    div.appendChild(cross);
    div.appendChild(p);
    popupSection.appendChild(div);

    cross.addEventListener("click", () => {
        div.remove();
    });

    setTimeout(function () {
        div.remove();
    }, 10000);
}