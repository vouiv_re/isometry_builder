"use strict";

import { createElt } from "./tool.js"

let cubeSize = 7;

let colors = document.getElementsByClassName("color");
let selectedColor = "black";

let tablePosition = {
    "height": 0,
    "width": 0,
    "depth": 0,
}

let mouseDown = 0;
document.body.onmousedown = function() { 
    ++mouseDown;
}
document.body.onmouseup = function() {
    --mouseDown;
}

for (const color of colors) {
    color.addEventListener("click", () => {
        for (const select of colors) {
            select.classList.remove("select");
        }
        color.classList.add("select");
        selectedColor = color.dataset.color;
        console.log(color.dataset.color);
    })
}

function createCubeArray(cSize = cubeSize) {
    let result = [];
    for (let x = 0; x < cSize; x++) {
        result.push([]);
        for (let y = 0; y < cSize; y++) {
            result[x].push([]);
            for (let z = 0; z < cSize; z++) {
                result[x][y].push("red");
            }
        }
    }

    return result;
}

function generateView(type, cSize = cubeSize) {
    let result = createElt("div", null, type, "camera");
    result.style.width = `${cSize * 25}px`;
    result.style.height = `${cSize * 25}px`;
    for (let x = 0; x < cSize; x++) {
        let row = createElt("div");
        for (let y = 0; y < cSize; y++) {
            let div = createElt("div", null, null, `cursor-${selectedColor}`);
            div.addEventListener("mouseover", () => {if (mouseDown) addTile(type, x, y)});
            div.addEventListener("mousedown", () => {addTile(type, x, y)});
            div.dataset.type = type;
            div.dataset.x = x;
            div.dataset.y = y;
            row.appendChild(div);
        }
        result.appendChild(row);
    }
    result.appendChild(displayAside(type));
    return result;
}

let topSection = document.getElementById("top");
let downSection = document.getElementById("down");
let isoDiv = document.getElementById("isometry");

// Il faut prendre à la plus petite unité de pixel entière, donc il faut que quart = int sinon il y a un décalage visuel
let size = 32;
let half = size / 2;
let quart = size / 4;

isoDiv.style.width = `${(cubeSize - 1) * half * 2 + size}px`;

topSection.appendChild(generateView("height"));
topSection.appendChild(generateView("width"));

downSection.insertBefore(generateView("depth"), isoDiv);

let cube = createCubeArray();

for (let x = 0; x < cubeSize; x++) {
    let heightRow = createElt("div", null, null, "depth");
    heightRow.style.left = `${(cubeSize - 1) * half}px`;
    heightRow.style.top = `${(cubeSize - 1) * half - half * x}px`;
    for (let y = 0; y < cubeSize; y++) {
        let depthRow = createElt("div", null, null, "width");
        depthRow.style.top = `${quart * y}px`;
        depthRow.style.left = `${-half * y}px`;
        for (let z = 0; z < cubeSize; z++) {
            let widthRow = null;
            // ici
            if ((z + y + x) % 2 === 0) {
                widthRow = createSVGCube("grey", size);
            } else {
                widthRow = createSVGCube("blue", size);
            }
            widthRow.style.top = `${quart * z}px`;
            widthRow.style.left = `${half * z}px`;
            depthRow.appendChild(widthRow);
        }
        heightRow.appendChild(depthRow);
    }
    isoDiv.appendChild(heightRow);
}

function changePosition(frame, value) {
    tablePosition[frame] = value;
    displayAside(frame);
    console.log(tablePosition);
}

function displayAside(type) {
    let frame = document.getElementById(type);
    let aside = null;

    if (frame === null) {
        aside = createElt("aside");
        aside.style.height = `${cubeSize * 25}px`;
    } else {
        aside = frame.lastChild;
        aside.innerHTML = "";
    }

    for (let x = 0; x < cubeSize; x++) {
        let position = createElt("p", `${x + 1}`);
        position.dataset.position = x;
        if (x === tablePosition[type]) {
            position.classList.add("selected");
        }
        position.addEventListener("click", () => {changePosition(type, x)});
        aside.appendChild(position);
    }

    return aside;
}

function addTile(type, x, y) {
    if (type === "height") {
        cube[x][y][tablePosition[type]] = selectedColor;
        console.log(`X: ${x}, Y: ${y}, Z: ${tablePosition[type]}`);
        document.getElementById("isometry").childNodes[x].childNodes[y].childNodes[tablePosition[type]].src = `../src/img/png/red-cube.png`;
        document.getElementById("height").childNodes[x].childNodes[y].style.backgroundColor = "#d04648";
    } else if (type === "width") {
        console.log(`X: ${tablePosition[type]}, Y: ${x}, Z: ${y}`);
    } else {
        console.log(`X: ${y}, Y: ${tablePosition[type]}, Z: ${x}`);
    }
    generateView();
    console.log(cube);
}

function createSVGCube(color = "#557fcb", s = 32) {
    let url = "http://www.w3.org/2000/svg";

    let svgElt = document.createElementNS(url, "svg");
    svgElt.setAttributeNS(null, "width", s);
    svgElt.setAttributeNS(null, "height", s);
    svgElt.style.display = "block";

    let g = document.createElementNS(url, "g");
    svgElt.appendChild(g);

    let top = document.createElementNS(url, "path");
    top.setAttributeNS(null, 'd', `M ${s/2} 0 L 0 ${s/4} L 0 ${s/(1+1/3)} L ${s/2} ${s} L ${s} ${s/(1+1/3)} L ${s} ${s/4} L ${s/2} 0 z `);
    top.setAttributeNS(null, 'fill', color);
    top.setAttributeNS(null, 'opacity', 1.0);
    
    let left = document.createElementNS(url, "path");
    left.setAttributeNS(null, 'd', `M 0 ${s/4} L 0 ${s/(1+1/3)} L ${s/2} ${s} L ${s/2} ${s/2} L 0 ${s/4} z `);
    left.setAttributeNS(null, 'fill', "black");
    left.setAttributeNS(null, 'opacity', 0.3);
    
    let right = document.createElementNS(url, "path");
    right.setAttributeNS(null, 'd', `M ${s} ${s/4} L ${s/2} ${s/2} L ${s/2} ${s} L ${s} ${s/(1+1/3)} L ${s} ${s/4} z `);
    right.setAttributeNS(null, 'fill', "black");
    right.setAttributeNS(null, 'opacity', 0.6);
    
    g.appendChild(top);
    g.appendChild(left);
    g.appendChild(right);

    return svgElt;
}
